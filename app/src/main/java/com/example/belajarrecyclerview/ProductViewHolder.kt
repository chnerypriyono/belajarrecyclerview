package com.example.belajarrecyclerview

import android.support.v7.widget.RecyclerView
import android.view.View
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.product_item.view.*

class ProductViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(productViewModel: ProductViewModel) {
        itemView.txtName.text = productViewModel.name
        itemView.txtPrice.text = productViewModel.price
        Glide.with(itemView.context).load(productViewModel.imageUrl)
                .dontAnimate().into(itemView.imgProduct)
    }
}

package com.example.belajarrecyclerview.multi

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView

import com.bumptech.glide.Glide
import com.example.belajarrecyclerview.R

class GameItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private val title: TextView
    private val subtitle: TextView
    private val image: ImageView
    private val context: Context

    init {
        context = itemView.context
        title = itemView.findViewById(R.id.title)
        subtitle = itemView.findViewById(R.id.subtitle)
        image = itemView.findViewById(R.id.image)
    }

    fun bind(productViewModel: GameViewModel, position: Int, itemView: View) {
        title.text = productViewModel.name
        subtitle.text = position.toString()
        Glide.with(context).load(productViewModel.imageUrl)
                .dontAnimate().into(image)
    }
}

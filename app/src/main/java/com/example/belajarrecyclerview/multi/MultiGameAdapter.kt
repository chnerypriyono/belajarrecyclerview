package com.example.belajarrecyclerview.multi

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.example.belajarrecyclerview.R

import java.util.ArrayList


class MultiGameAdapter : RecyclerView.Adapter<GameItemViewHolder>() {
    private val gameViewModelList = ArrayList<GameViewModel>()


    companion object {
        var LEFT_TYPE = 0
        var RIGHT_TYPE = 1
        var THUMBNAIL_TYPE = 2
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup,
                                    viewType: Int): GameItemViewHolder {
        if (viewType == LEFT_TYPE) {
            var view = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.game_item_left, viewGroup, false)
            return GameItemViewHolder(view)
        } else if (viewType == RIGHT_TYPE) {
            var view = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.game_item_right, viewGroup, false)
            return GameItemViewHolder(view)
        } else {
            var view = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.game_item_thumbnail, viewGroup, false)
            return GameItemViewHolder(view)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position > 0 && position < gameViewModelList.size) {
            if (position % 2 == 0) {
                LEFT_TYPE
            } else {
                RIGHT_TYPE
            }
        } else {
            THUMBNAIL_TYPE
        }

    }

    override fun onBindViewHolder(gameItemViewHolder: GameItemViewHolder,
                                  position: Int) {
        gameItemViewHolder.bind(gameViewModelList[position], position, gameItemViewHolder.itemView)
        var context = gameItemViewHolder.itemView.context
        val animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left)
        gameItemViewHolder.itemView.startAnimation(animation)
    }

    override fun getItemCount(): Int {
        return gameViewModelList.size
    }

    fun setData(gameViewModelList: List<GameViewModel>) {
        this.gameViewModelList.clear()
        this.gameViewModelList.addAll(gameViewModelList)
        notifyDataSetChanged()
    }

    fun addData(newGames: ArrayList<GameViewModel>) {
        var insertIndex = gameViewModelList.size
        this.gameViewModelList.addAll(newGames)
        notifyItemRangeInserted(insertIndex, newGames.size)
    }
}

package com.example.belajarrecyclerview

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

import java.util.ArrayList

class ProductAdapter : RecyclerView.Adapter<ProductViewHolder>() {
    private val itemList = ArrayList<ProductViewModel>()

    override fun onCreateViewHolder(viewGroup: ViewGroup,
                                    position: Int): ProductViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.product_item, viewGroup, false)
        return ProductViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ProductViewHolder,
                                  position: Int) {
        viewHolder.bind(itemList[position])
    }

    override fun getItemCount(): Int {
        return itemList.size
    }

    fun setData(dataList: List<ProductViewModel>) {
        this.itemList.clear()
        this.itemList.addAll(dataList)
        notifyDataSetChanged()
    }
}

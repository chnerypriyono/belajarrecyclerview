package com.example.belajarrecyclerview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.widget.Toast
import com.example.belajarrecyclerview.multi.GameViewModel
import com.example.belajarrecyclerview.multi.MultiGameAdapter

import java.util.ArrayList
import java.util.Random

class MainActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null
    var multiGameAdapter = MultiGameAdapter()
    var page = 1
    var hasNextPage = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    private fun initView() {
        recyclerView = findViewById(R.id.recyclerView)
        setLinearAdapter()
        //setGridAdapter()
    }

    private fun setLinearAdapter() {
        var layoutManager = LinearLayoutManager(this)
        recyclerView?.let {
            it.layoutManager = layoutManager
            it.adapter = multiGameAdapter
        }
        loadData(page)
        recyclerView?.addOnScrollListener(object : RecyclerView.OnScrollListener(){
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if(dy > 0) //check for scroll down
                {
                    val visibleItemCount = layoutManager.getChildCount()
                    val totalItemCount = layoutManager.getItemCount()
                    val pastVisiblesItems = layoutManager.findFirstVisibleItemPosition()

                    if (hasNextPage)
                    {
                        Log.d("belajarpos", visibleItemCount.toString()+ " "+ pastVisiblesItems +" "+ totalItemCount)
                        if ( (visibleItemCount + pastVisiblesItems) >= totalItemCount)
                        {
                            page++
                            loadData(page)
                        }
                    }
                }
            }
        })

    }

    private fun loadData(page: Int) {
        var gameViewModels = ArrayList<GameViewModel>()
        Toast.makeText(this,"sedang load page "+ page.toString(), Toast.LENGTH_LONG).show()
        when (page) {
            1 -> {
                gameViewModels.add(GameViewModel("Nintendo Switch", "https://cdn.wccftech.com/wp-content/uploads/2019/04/nintendo-switch-firmware-update-8.0.jpg"))
                gameViewModels.add(GameViewModel("Octopath Traveler", "https://66.media.tumblr.com/80f96bc07ef66262d939e68c8590c1d4/tumblr_pf1t3spxVC1riuctjo1_500.png"))
                gameViewModels.add(GameViewModel("Super Mario Oddysey", "https://banner2.kisspng.com/20180522/zjs/kisspng-super-mario-odyssey-super-mario-bros-nintendo-swi-super-mario-birthday-5b040403178c50.8616476815269898270965.jpg"))
                gameViewModels.add(GameViewModel("Legend of Zelda", "https://www.pngfind.com/pngs/m/228-2287227_zelda-breath-of-the-wild-png-link-breath.png"))
                gameViewModels.add(GameViewModel("Pokemon Sword & Shield", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS7Hkr8g6kfTFepcBIiW4P_vSmrp2deQN2sFhIfwITAgx55-oex"))
                gameViewModels.add(GameViewModel("Luigi's Mansion", "https://luigismansion.nintendo.com/assets/img/characters/char-luigi-1@2x.png"))
                multiGameAdapter.setData(gameViewModels)
                hasNextPage = true
            }
            2 -> {
                gameViewModels.add(GameViewModel("Animal Crossing", "https://banner2.kisspng.com/20180529/jez/kisspng-animal-crossing-amiibo-festival-animal-crossing-animal-crossing-5b0de7109f5501.7501215415276377766526.jpg"))
                gameViewModels.add(GameViewModel("Overcooked 2", "https://www.team17.com/wp-content/uploads/2018/06/WebsiteMain-1260x709.jpg"))
                multiGameAdapter.addData(gameViewModels)
                hasNextPage = true
            }
            3 -> {
                gameViewModels.add(GameViewModel("Splatoon 2", "https://cdn.imgbin.com/22/12/12/imgbin-splatoon-2-nintendo-switch-wii-u-nintendo-Q17edvyLh9bTye1JjdxZw9s8C.jpg"))
                gameViewModels.add(GameViewModel("Arms", "https://banner2.kisspng.com/20180405/ieq/kisspng-arms-splatoon-2-wii-nintendo-switch-nintendo-5ac5c873d2ee59.027467471522911347864.jpg"))
                multiGameAdapter.addData(gameViewModels)
                hasNextPage = true
            }
            4 -> {
                gameViewModels.add(GameViewModel("Mario Kart 8", "https://cdn.imgbin.com/25/2/8/imgbin-mario-kart-8-deluxe-new-super-mario-bros-2-mario-kart-super-circuit-mario-super-mario-world-yArCR7M2bczEHbeEvukq7Zv5L.jpg"))
                gameViewModels.add(GameViewModel("Mario Tennis Aces", "https://banner2.kisspng.com/20180505/qzq/kisspng-mario-tennis-aces-mario-tennis-power-tour-luigi-ace-5aeda5e673fac6.0476743315255239424751.jpg"))
                multiGameAdapter.addData(gameViewModels)
                hasNextPage = false
            }
        }
    }

    private fun setGridAdapter() {
        var productViewModelList = ArrayList<ProductViewModel>()
        val productAdapter = ProductAdapter()
        recyclerView?.let {
            it.layoutManager = GridLayoutManager(this, 2)
            it.adapter = productAdapter
        }

        val random = Random()
        for (i in 1..99) {
            val imageUrl = imageList[random.nextInt(imageList.size)]
            productViewModelList.add(ProductViewModel(
                    "Baju Keren" + i.toString(), "Rp10000" + i.toString(), imageUrl))
        }
        productAdapter.setData(productViewModelList)
    }

    companion object {

        private val imageList = arrayOf(
                "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2019/5/28/215949803/215949803_a2e6fec6-9465-47c4-949d-5a9aca0c2ea0_700_700.jpg",
                "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2018/2/12/0/0_39ad6df5-6cbc-481c-9d37-ba56a0fd23fc_790_790.jpg",
                "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2017/12/20/25358479/25358479_4662e848-db77-415b-80c9-c152bc98ace4_877_877.jpg",
                "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2019/5/30/281780431/281780431_d27e2126-7e7d-4144-8b24-620cefe09632_700_700.jpg",
                "https://ecs7.tokopedia.net/img/cache/200-square/product-1/2018/2/14/16409751/16409751_afcd586a-a0fa-4a83-8ce7-59d124a957e7_1500_1500.jpg"
        )
    }
}
